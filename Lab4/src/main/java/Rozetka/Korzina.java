package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 12.06.2016.
 */
public class Korzina {

    private WebDriver driver;

    public Korzina(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }



    @FindBy(xpath = ".//h2[@class='empty-cart-title inline sprite-side']")
    public WebElement korzina_empty;


}



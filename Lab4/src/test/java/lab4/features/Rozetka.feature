@testall @onlyrozetka
Feature: Rozetka testing

  Background:
    Given I am on Rozetka start page

  Scenario: 001 checking of logo
    Then I see logo rozetka

  @twotest
  Scenario: 002 checking of Apple menu
    Then I see Apple section
  @twotest
  Scenario: 003 checking of Apple menu
    Then I see MPT section

  Scenario: 004 checking of available cities
    When I click on the City link
    Then I see pop-up with Kharkiv, Kiev and Odessa cities

  Scenario: 005 checking of bin
    When I click Korzina link
    Then I see that Korzina is empty
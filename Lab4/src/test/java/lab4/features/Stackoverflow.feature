@testall @onlystackoverflow
Feature: Stackoverflow testing

  Background:
    Given I am on Stackoverflow main page

  @twotest
  Scenario: 001 checking of featured number
    Then I see that featured number is more than 300


  Scenario: 002 checking of social network buttons
    When I click SignUp link
    Then I see Facebook and Google buttons

  @twotest
  Scenario: 003 checking of question's day
    When I click first Question link
    Then I see that question was asked today
package lab4.steps;

import cucumber.api.CucumberOptions;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import lab4.run.TestRunner;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
/**
 * Created by фильчаковы on 13.06.2016.
 */
public class MyStepdefs {

    @Given("^I am on Rozetka start page$")
    public void RozetkaStartpage() throws Throwable {
        if (!TestRunner.driver.getCurrentUrl().equals("http://rozetka.com.ua/")) {
            TestRunner.driver.get("http://rozetka.com.ua/");
        }
    }

    @Then("^I see logo rozetka$")
    public void LogoRozetka() throws Throwable {
        assertTrue("Logo is not displayed", TestRunner.startPageRozetka.label_rozetka.isDisplayed());
    }

    @Then("^I see Apple section$")
    public void AppleSection() throws Throwable {
        assertTrue("Apple is not displayed", TestRunner.startPageRozetka.apple_element.isDisplayed());
    }

    @Then("^I see MPT section$")
    public void MP3Section() throws Throwable {
        assertTrue("MP3 section is not displayed", TestRunner.startPageRozetka.mp3_element.isDisplayed());
    }

    @When("^I click on the City link$")
    public void CityLink() throws Throwable {
        TestRunner.goroda = TestRunner.startPageRozetka.navigateToGoroda();
    }

    @Then("^I see pop-up with Kharkiv, Kiev and Odessa cities$")
    public void CitiesDisplayed() throws Throwable {
        assertTrue("Kharkiv is not displayed", TestRunner.goroda.pervii_gorod.isDisplayed());
        assertTrue("Odessa is not displayed", TestRunner.goroda.vtoroi_gorod.isDisplayed());
        assertTrue("Kiev is not displayed", TestRunner.goroda.tretii_gorod.isDisplayed());
    }

    @When("^I click Korzina link$")
    public void KorzinaLink() throws Throwable {
        TestRunner.korzina = TestRunner.startPageRozetka.navigateToCorzina();
    }

    @Then("^I see that Korzina is empty$")
    public void Korzinaempty() throws Throwable {
        assertTrue("Korzina is not empty", TestRunner.korzina.korzina_empty.isDisplayed());
    }

    @Given("^I am on Stackoverflow main page$")
    public void StackoverflowStartPagenow() throws Throwable {
        if (!TestRunner.driver.getCurrentUrl().equals("http://stackoverflow.com/")) {
            TestRunner.driver.get("http://stackoverflow.com/");
        }
    }

    @Then("^I see that featured number is more than (\\d+)$")
    public void FeaturedMoreThanNumber(int number) throws Throwable {
        TestRunner.startPageStack.TestKoli4ectvaFeatured();
    }

    @When("^I click SignUp link$")
    public void SignUpLink() throws Throwable {
        TestRunner.signUp = TestRunner.startPageStack.navigateToSignUp();
    }

    @Then("^I see Facebook and Google buttons$")
    public void FacebookAndGoogleButtons() throws Throwable {
        assertTrue("Google button is not displayed", TestRunner.signUp.lnk_google.isDisplayed());
        assertTrue("Facebook button is not displayed", TestRunner.signUp.lnk_menu_facebook.isDisplayed());
    }

    @When("^I click first Question link$")
    public void FirstQuestionLink() throws Throwable {
        TestRunner.topQuestions = TestRunner.startPageStack.navigateToQuestion();
    }

    @Then("^I see that question was asked today$")
    public void QuestionToday() throws Throwable {
        assertTrue("Question wasn't asked today", TestRunner.topQuestions.text_today.isDisplayed());
    }


}



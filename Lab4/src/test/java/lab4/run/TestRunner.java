package lab4.run;
import Rozetka.Goroda;
import Rozetka.Korzina;
import Rozetka.StartPageRozetka;
import Rozetka.*;
import StackoverflowPages.SignUp;
import StackoverflowPages.StartPageStack;
import StackoverflowPages.TopQuestions;
import StackoverflowPages.SignUp;
import StackoverflowPages.StartPageStack;
import StackoverflowPages.TopQuestions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)

@CucumberOptions (
        features = "src/test/java/lab4/features",
        glue = "lab4/steps",
        tags = "@testall, @twotest, @onlyrozetka, @onlystackoverflow"
)


/**
 * Created by фильчаковы on 13.06.2016.
 */
public class TestRunner {

    public static WebDriver driver;
    public static StartPageRozetka startPageRozetka;
    public static Goroda goroda;
    public static Korzina korzina;
    public static StartPageStack startPageStack;
    public static TopQuestions topQuestions;
    public static SignUp signUp;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        startPageRozetka = new StartPageRozetka(driver);
        startPageStack = new StartPageStack(driver);

    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}

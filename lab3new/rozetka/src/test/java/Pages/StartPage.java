package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 12.06.2016.
 */
public class StartPage {
    private WebDriver driver;
    public StartPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy(xpath = ".//div/img")
    public WebElement label_rozetka;

    @FindBy(xpath = ".//li/a[contains(text(),'Apple')]")
    public WebElement apple_element;

    @FindBy(xpath = ".//*[@id='m-main']/li[3]/a[contains(text(),'MP3')]")
    public WebElement mp3_element;

    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement link_goroda;

    @FindBy(xpath = ".//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement lnk_menu_korzina;

    public Korzina navigateToCorzina() {
        lnk_menu_korzina.click();
        return  new Korzina(driver);
    }

    public Goroda navigateToGoroda() {
        link_goroda.click();
        return new Goroda(driver);
    }

}

package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 12.06.2016.
 */
public class Goroda {
    private WebDriver driver;

    public Goroda(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[2]/div/div/div/a[contains(text(),'Харьков')]")
    public WebElement pervii_gorod;

    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[2]/div/div/div/a[contains(text(),'Киев')]")
    public WebElement vtoroi_gorod;

    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[2]/div/div/div/a[contains(text(),'Одесса')]")
    public WebElement tretii_gorod;
}

package Testing;

import Pages.Goroda;
import Pages.Korzina;
import Pages.StartPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;

/**
 * Created by фильчаковы on 12.06.2016.
 */
public class Tests {
    private static WebDriver driver;
    private static Korzina korzina;
    private static StartPage startpage;
    private static Goroda gorod;

    @BeforeClass
    public static void SetUp() throws Exception{
        driver =new FirefoxDriver();
        driver.get("http://rozetka.com.ua");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        startpage=new StartPage(driver);
            }

    @AfterClass
    public  static void tearDown(){

        driver.close();
    }

    @Test
    public void TestLabelStartPage() throws Exception{

        assertTrue("Label is missing", startpage.label_rozetka.isDisplayed());
    }
    @Test
    public void TestKorzinaisEmpty() throws Exception{
        korzina=startpage.navigateToCorzina();
        assertTrue("korzina is not displayed", korzina.korzina_empty.isDisplayed());
    }
    @Test
    public void TestAppleLink() throws Exception{

        assertTrue("apple not displayed", startpage.apple_element.isDisplayed());
    }
    @Test
    public void TestMP3Link() throws Exception{

        assertTrue("MP3 is not displayed", startpage.mp3_element.isDisplayed());
    }
    @Test
    public void TestGoroda() throws Exception{
        gorod=startpage.navigateToGoroda();
        assertTrue("gorodov net", gorod.pervii_gorod.isDisplayed());
        assertTrue("gorodov net", gorod.vtoroi_gorod.isDisplayed());
        assertTrue("gorodov net", gorod.tretii_gorod.isDisplayed());
    }
}

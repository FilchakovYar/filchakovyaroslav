package Testing;

import Pages.SignUp;
import Pages.StartPage;
import Pages.TopQuestions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by фильчаковы on 12.06.2016.
 */
public class Tests {
    private static WebDriver driver;
    private static StartPage startpage;
    private static SignUp singup;
    private static TopQuestions topquestions;


    @BeforeClass
    public static void SetUp(){
        driver =new FirefoxDriver();
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        startpage = new StartPage(driver);

    }
    @AfterClass
    public  static void tearDown(){
        driver.close();
    }

    @Before
    public void Before1(){
        if(!driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            driver.get("http://stackoverflow.com/");
    }

    @Test
    public void TestLabelStartPage(){
        assertTrue("content is absent", startpage.label_stack.isDisplayed());
    }

    @Test
    public void TestKoli4ectva(){
        startpage.TestKoli4ectvaFeatured();

    }
    @Test
    public void TestSignUp() {
        singup = startpage.navigateToSignUp();
        assertTrue("Google is not displayed", singup.lnk_google.isDisplayed());
        assertTrue("Facebook is not displayed", singup.lnk_menu_facebook.isDisplayed());
    }
    @Test
    public void TestQuestionToday() {
        topquestions = startpage.navigateToQuestion();
        assertTrue("Not Today", topquestions.text_today.isDisplayed());
    }

}

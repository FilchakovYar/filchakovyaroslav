package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by фильчаковы on 12.06.2016.
 */
public class StartPage {
    private WebDriver driver;

    public StartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div/a[contains(text(),'Stack Overflow')]")
    public WebElement label_stack;

    @FindBy(xpath = ".//*[@id='tabs']/a[2]/span")
    public WebElement link_featured;

    @FindBy(xpath = ".//span/a[text()='sign up']")
    public WebElement link_signup;

    @FindBy(xpath = ".//div[2]/h3/a")
    public WebElement link_anyquestion;

    public SignUp navigateToSignUp() {
        link_signup.click();
        return new SignUp(driver);
    }

    public TopQuestions navigateToQuestion() {
        link_anyquestion.click();
        return new TopQuestions(driver);
    }

    public void TestKoli4ectvaFeatured() {
        Integer kol = Integer.parseInt(String.valueOf(link_featured.getText()));
        assertTrue("menwe 300", kol > 300);
    }
}

